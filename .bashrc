


if [ -z "$GIT_HOME_REPO" ] ; then
    return
fi

if [[ -e $GIT_HOME_REPO/bin ]] ; then
    export PATH=$GIT_HOME_REPO/bin:$GIT_HOME_REPO/bin/scripts:$GIT_HOME_REPO/npm/node_modules/.bin:$PATH
fi

if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


if [[ -e $GIT_HOME_REPO/man ]] ; then
    export MANPATH=$GIT_HOME_REPO/man:$MANPATH
fi

function mktmp() {
    eval `_mktmp "$@"`
}

function pathman() {
    eval `_pathman "$@"`
}

export RIPGREP_CONFIG_PATH=$GIT_HOME_REPO/.ripgreprc
export BAT_CONFIG_PATH=$GIT_HOME_REPO/.config/bat/config
alias lx=exa
source $GIT_HOME_REPO/scripts/rg.bash
source $GIT_HOME_REPO/scripts/key-bindings.bash
source $GIT_HOME_REPO/scripts/fd.bash-completion
source $GIT_HOME_REPO/scripts/fzf.bash-completion
source $GIT_HOME_REPO/scripts/exa.bash
source $GIT_HOME_REPO/scripts/bat.bash
source $GIT_HOME_REPO/scripts/hyperfine.bash


export FZF_ALT_C_COMMAND="command fd -td"


export SKIM_DEFAULT_COMMAND="fd --type f"
export SKIM_DEFAULT_OPTIONS="--color 16"

function fman() {
    man -w |tr : '\n' |xargs -I {} fd --type f . {} --print0 |sk --read0 --preview 'man {}' --bind 'ctrl-m:execute(man {})' --color 16
}

# If vim is installed, use that as the editor
which vim > /dev/null
if [ $? -eq 0 ]; then
    export EDITOR=vim
fi

export VIMINIT='source $GIT_HOME_REPO/.vimrc'
export STARSHIP_CONFIG=$GIT_HOME_REPO/.config/starship.toml


eval "$(starship init bash)"

