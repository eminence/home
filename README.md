Clone this repo somewhere and add this to your bashrc file:


    if [[ -e ~/.git-home-repo/.bashrc ]] ; then
        export GIT_HOME_REPO=~/.git-home-repo/
        . $GIT_HOME_REPO/.bashrc
    fi


Or if using tcsh:

    if ( -e ~/.git-home-repo/.cshrc ) then
        setenv GIT_HOME_REPO ~/.git-home-repo
        source $GIT_HOME_REPO/.cshrc
    endif
