

if (! $?GIT_HOME_REPO) then
    exit 0
endif

if ( -e $GIT_HOME_REPO/bin ) then
    setenv PATH $GIT_HOME_REPO/bin:$GIT_HOME_REPO/bin/scripts:$GIT_HOME_REPO/npm/node_modules/.bin:$PATH
endif

if ( ! $?prompt ) then   # test if non-interactive (script or remote)
    exit 0
endif

if ( -e $GIT_HOME_REPO/man ) then
    setenv MANPATH $GIT_HOME_REPO/man:$MANPATH
endif

alias mktmp 'eval `_mktmp \!*`'
alias pathman 'eval `_pathman \!*`'

setenv RIPGREP_CONFIG_PATH $GIT_HOME_REPO/.ripgreprc
setenv BAT_CONFIG_PATH $GIT_HOME_REPO/.config/bat/config
setenv STARSHIP_CONFIG $GIT_HOME_REPO/.config/starship.toml
alias lx exa
