import subprocess
import json
import http.client
import urllib.parse


# First, get LSF object info

p = subprocess.Popen(["git", "ls-tree", "HEAD", "bin/git-lfs"], stdout=subprocess.PIPE)
stdout = p.communicate()[0].decode()
git_lfs_hash =  stdout.split()[2]

p = subprocess.Popen(["git", "cat-file", "-p", git_lfs_hash], stdout=subprocess.PIPE)
stdout = p.communicate()[0].decode()

# version https://git-lfs.github.com/spec/v1
# oid sha256:46e8a9b734a973482cbc5932872012434e547a6425dd634548b2e2b8d9a612f7 
# size 7778976

for line in stdout.splitlines():
    line = line.strip()
    if line.startswith("oid "):
        sha = line.split()[1]
        shatype, sha = sha.split(":")
    elif line.startswith("size "):
        size = int(line.split()[1])


print(shatype, sha, size)

# ssh://git@code.em32.net/eminence/home.git 
p = subprocess.Popen(["git", "remote", "get-url", "origin"], stdout=subprocess.PIPE)
stdout = p.communicate()[0].decode()
if stdout.startswith("ssh://"):
    url = stdout[6:]
    user, proj = url.split("/",1)

    # ssh git@code.em32.net git-lfs-authenticate eminence/home.git download
    p = subprocess.Popen(["ssh", user, "git-lfs-authenticate", proj, "download"], stdout=subprocess.PIPE)
    stdout = p.communicate()[0].decode()

    lfs_auth = json.loads(stdout)
    print(lfs_auth)

    url = urllib.parse.urlparse(lfs_auth['href'])

    cl = http.client.HTTPSConnection(url.netloc, port=url.port or 443)
    headers = {
            "Accept": "application/vnd.git-lfs+json",
            "Content-Type": "application/vnd.git-lfs+json"
        }

    headers.update(lfs_auth['header'])

    body = {
            "operation": "download",
            "transfers": [ "basic" ],
            "objects": [{
                "oid": sha,
                "size": size
                }]

            }

    cl.request("POST", url.path + "objects/batch", json.dumps(body), headers)
    resp = cl.getresponse()
    resp_obj = json.loads(resp.read().decode())
    
    resp_obj = resp_obj['objects'][0]['actions']['download']

    url = urllib.parse.urlparse(resp_obj['href'])
    headers = resp_obj['header']
    cl = http.client.HTTPSConnection(url.netloc, port=url.port or 443)
    cl.request("GET", url.path, headers = headers)
    resp = cl.getresponse()

    lfs_binary = resp.read()
    with open("git-lfs", "wb") as fobj:
        fobj.write(lfs_binary)
