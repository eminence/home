
set nocompatible
"syn on
set number
set modeline
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smartindent
set cindent
set expandtab
set ruler
set smarttab
set ai "auto indent
set foldenable
set foldmethod=marker
set incsearch
set ignorecase
set showcmd
set smartcase
set bs=indent,eol,start
set hlsearch

set laststatus=2
set wildmenu
set wildignore=*.o,*.~,*.pyc
set showmatch
set mat=2
set mouse=a


au bufread,BufNewFile *.jad set filetype=java
au BufRead,BufNewFile *.cron set filetype=crontab
au BufRead,BufNewFile *.lzx set filetype=lzx
au BufRead,BufNewFile *.C set ts=8
au BufRead,BufNewFile *.cpp set ts=8
au BufRead,BufNewFile *.h set ts=8
au BufRead,BufNewFile *.gv set filetype=dot

au BufRead,BufNewFile *.yml set ts=2 sw=2


" shortcut for diffget
nmap d> <ESC>:diffget<CR>

" Insert the current filename, minus extension
inoremap \fn <C-R>=expand("%:t:r")<CR>
" with extension
inoremap \fe <C-R>=expand("%:t")<CR>
" directory
inoremap \fd <C-R>=expand("%:p:h")<CR>
" PWD
inoremap \cwd <C-R>=getcwd()<CR>

set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»


function! SourceIfExists(file)
  if filereadable(expand(a:file))
    exe 'source' a:file
  endif
endfunction

" Any local/site specific stuff should go here
call SourceIfExists("~/.vimrc")
call SourceIfExists("~/.vim/site.vim")

